# README #

Steps to open and run the console program. 

### What is this repository for? ###

* DocuSign Assignment
* Version 1.0

### How do I get set up? ###

* Check out the solution using the "SourceTree" https://www.sourcetreeapp.com/.
* Open .sln file with VS 2015  (could be later year as well).
* Run the program (F5).
* Enter commands and will provide the corresponding input. 
* There are Unit Tests as well.