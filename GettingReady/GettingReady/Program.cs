﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GettingReady
{
    public class Program
    {
        //The driver takes the userinput from command prompt and sends to "ProcessUserInput" method. 
        static void Main(string[] args)
        {
            String  userInput = Console.ReadLine();
            Console.WriteLine(ProcessUserInput(userInput));
            Console.ReadLine();
        }

        /// <summary>
        ///  Enum class was chosen to represent the 2 different possibilities.
        ///  It is fairly easy to add any other options if necessary at any time !
        /// </summary>
        public enum OutsideTemp
        {
            COLD,
            HOT
        };

        /// <summary>
        ///  Defined a readonly constant variable   
        ///  Created a list of commands to hold all the available options to dress up with.
        /// </summary>
        public static readonly String FAIL = "fail";
        public static Dictionary<int, Dictionary<string, String>> mapper;
        public static List<String> commandsDesc = new List<string>
        {
            "sandals", "boots", "sun visor", "hat", FAIL, "socks", "t-shirt", "shirt", FAIL, "jacket", "shorts", "pants", "leaving house", "leaving house", "Removing PJs", "Removing PJs"
        };

        /// <summary>
        /// Receives the userInput as an argument, runs through the validation rules 
        /// and outputs the corresponding return value.
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        public static String ProcessUserInput(String line)
        {
            //loop through and map the commandIds to the OutSideTemp value
            mapper = new Dictionary<int, Dictionary<String, String>>();
            Dictionary<string, string> retVal;

            for (int i = 0, j = 1; i < commandsDesc.Count; ++i)
            {
                retVal = new Dictionary<string, string>();
                retVal.Add(OutsideTemp.COLD.ToString(), commandsDesc.ElementAt(i));
                retVal.Add(OutsideTemp.HOT.ToString(), commandsDesc.ElementAt(i++));
                mapper.Add(j++, retVal);
            }

            HashSet<int> commandIds = new HashSet<int>(); 
            StringBuilder output = new StringBuilder();    

            String[] inputParams = line.Split();
            String rest = String.Join("", inputParams);

            //gets the first element of the input string e.i HOT OR COLD
            string key = inputParams[0];
            for (var i = 1; i < inputParams.Length; ++i)
            {
                int val = 0;
                Int32.TryParse(inputParams[i].TrimEnd(new char[] { ',' }), out val);

                //Validation Rule #1 and 2: You need to be in your house with your pajamas on, else it is  "FAIL".
                if (i == 1 && val != 8)
                {
                    output.Append(FAIL);
                    return output.ToString();
                }

                //Validation Rules #6 and #7: Socks must be put on before shoes & Pants must be put on before shoes.
                else if (val == 1)  
                {
                    if (key == OutsideTemp.COLD.ToString() && (!commandIds.Contains(3) || !commandIds.Contains(6)))
                    {
                        //Socks and Pants must be worn before shoes
                        output.Append(FAIL);
                        return output.ToString();
                    }
                    else if (key == OutsideTemp.HOT.ToString() && (!commandIds.Contains(6)))
                    {
                        //Pants must be worn before shoes
                        output.Append(FAIL);
                        return output.ToString();
                    }
                }

                // Validation Rule #8: The shirt must be put on before the headwear or jacket.
                else if (val == 2 || val == 5)     
                {
                    if (!commandIds.Contains(4))
                    {
                        output.Append(FAIL);
                        return output.ToString();
                    }
                }

                // Validation Rule  #9: You cannot leave the house until all items of clothing are on (except socks and a jacket when it’s hot)
                else if (val == 7)      
                {
                    if ((key == OutsideTemp.COLD.ToString() && commandIds.Count != 7) || (key == OutsideTemp.HOT.ToString() && commandIds.Count < 5))
                    {
                        output.Append(FAIL);
                        return output.ToString();
                    }
                }

                // Validation Rule  #10: If an invalid command is issued, respond with “fail” and stop processing commands 
                // e.i this wil be out of range command since 1 - 8 posibilities only.
                else if (val > 8 || val < 1)
                {
                    output.Append(FAIL);
                    return output.ToString();
                }

                var result = mapper.Where(q => q.Key == val)
                              .SelectMany(p => p.Value)
                              .Where(f => f.Key == key)
                              .Select(d => d.Value)
                              .ToList();

                // Validation Rule #3: 	Only 1 piece of each type of clothing may be put on
                if (commandIds.Contains(val))
                {
                    output.Append(FAIL);
                    return output.ToString();
                }

                // Validation #9: respond with “fail” and stop processing commands.
                else
                {
                    String resp = result[0];

                    if (resp == FAIL)
                    {
                        output.Append(FAIL);
                        return output.ToString();
                    }

                    commandIds.Add(val);

                    if (val != 7)
                    {
                        output.Append(resp);
                        output.Append(", ");
                    }
                    else
                    {
                        output.Append(resp);
                    }
                }
            }
            return output.ToString();
        }
    }
}
