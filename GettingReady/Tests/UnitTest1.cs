﻿using System;
using System.ComponentModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GettingReady;

namespace Tests
{
    [TestClass]
    public class UnitTest1
    {               
        //* RULE #1 & #2 *//                   
        [TestCategory("GettingReady"), TestMethod] 
        public void RemovePajamasFirst()
        {
            String userInput = "HOT 7, 1";
            String output = Program.ProcessUserInput(userInput);
            Assert.AreEqual("fail", output);
        }

        //* RULE  #3 *//                     
        [TestCategory("GettingReady"), TestMethod]
        public void OnePieceOfEachType()
        {
            String userInput = "HOT 8, 6, 1, 1";
            String output = Program.ProcessUserInput(userInput);
            Assert.AreEqual("Removing PJs, shorts, sandals, fail", output);
        }

        //* RULE  #4 *//
        [TestCategory("GettingReady"), TestMethod]
        public void NoSocksWhenHot()
        {
            String userInput = "HOT 8, 3";
            String output = Program.ProcessUserInput(userInput);
            Assert.AreEqual("Removing PJs, fail", output);
        }

        //* RULE  #5 *//
        [TestCategory("GettingReady"), TestMethod]
        public void NoJacketWhenHot()
        {
            String userInput = "HOT 8, 5";
            String output = Program.ProcessUserInput(userInput);
            Assert.AreEqual("Removing PJs, fail", output);
        }

        //* RULE  #6 *//
        [TestCategory("GettingReady"), TestMethod]
        public void PutSocksAftershoes()
        {
            String userInput = "COLD 8, 6, 1, 2, 3";
            String output = Program.ProcessUserInput(userInput);
            Assert.AreEqual("Removing PJs, shorts, fail", output);
        }

        //* RULE  #7 *//
        [TestCategory("GettingReady"), TestMethod]
        public void PutPantsAfterShoes()
        {
            String userInput = "COLD 8, 1, 6, 2, 3";
            String output = Program.ProcessUserInput(userInput);
            Assert.AreEqual("Removing PJs, fail", output);
        }

        //* RULE  #8 *//
        [TestCategory("GettingReady"), TestMethod]
        public void PutShirtAfterJacket()
        {
            String userInput = "COLD 8, 3, 5, 4";
            String output = Program.ProcessUserInput(userInput);
            Assert.AreEqual("Removing PJs, fail", output);
        }

        //* RULE  #8 *//
        [TestCategory("GettingReady"), TestMethod]
        public void PutShirtAfterHeadwear()
        {
            String userInput = "HOT 8, 6, 2, 4";
            String output = Program.ProcessUserInput(userInput);
            Assert.AreEqual("Removing PJs, shorts, fail", output);
        }

        //* RULE  #9 *//
        [TestCategory("GettingReady"), TestMethod]
        public void GotDressedUpAndLeftHouse()
        {
            String userInput = "HOT 8, 6, 4, 2, 1, 7";
            String output = Program.ProcessUserInput(userInput);
            Assert.AreEqual("Removing PJs, shorts, t-shirt, sun visor, sandals, leaving house", output);
        }

        //* RULE  #10 *//
        [TestCategory("GettingReady"), TestMethod]
        public void InvalidInput()
        {
            String userInput = "HOT 8, 6, 6";
            String output = Program.ProcessUserInput(userInput);
            Assert.AreEqual("Removing PJs, fail", output);
        }

    }
}

